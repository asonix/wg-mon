#!/usr/bin/env bash

function require() {
    if [ "$1" = "" ]; then
        echo "input '$2' required"
        print_help
        exit 1
    fi
}

function print_help() {
    echo "build.sh"
    echo ""
    echo "Usage:"
    echo "	build.sh [tag] [branch] [push]"
    echo ""
    echo "Args:"
    echo "	tag: The git tag to be applied to the repository and docker build"
    echo "	branch: The git branch to use for tagging and publishing"
    echo "	push: Whether or not to push the image"
    echo ""
    echo "Examples:"
    echo "	./build.sh v0.3.0-alpha.13 main true"
    echo "	./build.sh v0.3.0-alpha.13-shell-out asonix/shell-out false"
}

function build_image() {
    repo=$1
    tag=$2
    arch=$3
    push=$4

    sudo docker build \
        --pull \
        --build-arg TAG=$tag \
        --build-arg ARCH_REPO=$arch \
        -t asonix/$repo:$arch-$tag \
        -f Dockerfile \
        .

    sudo docker tag asonix/$repo:$arch-$tag asonix/$repo:$arch-latest

    if [ "$push" == "true" ]; then
        sudo docker push asonix/$repo:$arch-$tag
        sudo docker push asonix/$repo:$arch-latest
    fi
}

function copy_binary() {
    repo=$1
    tag=$2
    arch=$3

    mkdir -p artifacts/

    sudo docker run \
        --rm -it \
        -u "${UID}:${GID}" \
        -v "$(pwd)/artifacts:/mnt" \
        asonix/$repo:$arch-$tag \
        cp /opt/build/binary /mnt/${repo}-${arch}
}

function push_manifest() {
    repo=$1
    tag=$2

    sudo docker manifest create asonix/$repo:$tag \
        -a asonix/$repo:arm64v8-$tag \
        -a asonix/$repo:arm32v7-$tag \
        -a asonix/$repo:amd64-$tag

    sudo docker manifest annotate asonix/$repo:$tag \
        asonix/$repo:arm64v8-$tag \
        --os linux \
        --arch arm64 \
        --variant v8

    sudo docker manifest annotate asonix/$repo:$tag \
        asonix/$repo:arm32v7-$tag \
        --os linux \
        --arch arm \
        --variant v7

    sudo docker manifest annotate asonix/$repo:$tag \
        asonix/$repo:amd64-$tag \
        --os linux \
        --arch amd64

    sudo docker manifest push asonix/$repo:$tag --purge
}

# Creating the new tag
repo=wg-mon
new_tag="$1"
branch="$2"
push="${3:-false}"

require "$new_tag" "tag"
require "$branch" "branch"
require "$push" "push"

if ! sudo docker run --rm -it arm64v8/alpine:3.11 /bin/sh -c 'echo "docker is configured correctly"'
then
    echo "docker is not configured to run on qemu-emulated architectures, fixing will require sudo"
    sudo docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
fi

git commit -m "Version $new_tag"

set -xe

git tag $new_tag

git push origin $new_tag
git push

build_image $repo $new_tag arm64v8 $push
build_image $repo $new_tag arm32v7 $push
build_image $repo $new_tag amd64 $push

copy_binary $repo $new_tag arm64v8
copy_binary $repo $new_tag arm32v7
copy_binary $repo $new_tag amd64

if [ "$push" == "true" ]; then
    push_manifest $repo $new_tag
    push_manifest $repo latest
fi
